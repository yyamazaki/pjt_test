# README
PJT(Perfect Jouraku Tree)の動作確認を行います。
50のテストケースのうち、6割以上を正解した場合を合格とみなします。

## USAGE
以下の手順で、pjtの動作確認が行えます。
`${pjt_directory}`には、対象の`pjt`があるdirectoryのパスを入れてください。
```sh
./pjt_test.sh ${pjt_directory}
```
また、出力が異なる場合とは別に、実行時間が長くなる場合にもエラーとなるようにしています。
実行時間の最大値はデフォルトは10秒となっており、変更したい場合は`${timemax}`に異なる値を代入すれば良い。

`-t`オプションをつけることでテストケースの総数が50になるようにランダムなテストの生成及び回答の生成が行われます。回答は'-t-オプション指定時のパスにあるPJTを元に生成されるので、信頼性が高いPJTを指定する必要があります。
```sh
./pjt_test.sh -t ${pjt_directory}
```
テストケースの内容については`./pjt`の後に続く引数を`testCases_*`に記載してありますので、適宜修正を行ってください。

### VARIABLES
pjt_test.shに書いてある変数の内容について示します。

|Name|Job|
|:-:|:-:|
|max_score|最大の点数|
|time_max|PJTに不備があると判断するまでの時間|
|passing_rate|合格率|

### EXPLANATION
- time_maxを設定することで、無限に終わらないコードも強制的に終了させるまでの時間を決めることができます。実際テストケースにおいては5分程度で正しい答えに到達することができるPJTの例もあるので状況に応じて変える必要があります。

- passing_rateは合格率を操作する変数です。例年は60%が合格率だったが、このテストケースではPJT'-s'オプションが全く機能しなくても、合格率を超えてしまう可能性があるので、変更する必要があると考えられます。(2019年度4月現在)



### DIRECTORY
ディレクトリ構造は以下に示す通りである。
* `output`には、`pjt_test`実行時に不正解だったテストケースにおける出力が保管される。
* `test_case/answer_*.txt`には、各テストケースにおける回答が記載されている。
* `test_case/testCases_*.txt`には、テストケース実行時に渡す引数が記載されている。
* `test_case/test_random*.txt`には、ランダム生成された文字列が記載されている。
```
├── README.md
├── output
│   └── output_file*.txt
├── pjt_test.sh
├── random_words.py
└── test_case
    ├── answer_file*.txt
    ├── answer_file_randomXX.txt
    ├── answer_stdin*.txt
    ├── testCases_file.txt
    ├── testCases_file_random.txt
    ├── testCases_stdin.txt
    └── test_random*.txt
```

## PJT OUTPUT FORMAT
`pjt`コマンドの出力形式は以下の通りになることが想定されていて、`---BinSTree---`より下の文字列が一致することを`diff`コマンドを用いて判定しています。
```sh
% ./pjt
a aaa aa
---BinSTree---
a
aa
aaa
```
また、オプションエラーの場合は`usage`という文字列が出力されていることを、オプションの処理によりノードが全て失われた場合は`no`という文字列が出力されていることを`grep -i`を用いて確認しています。

## REQUIREMENTS
* gawk
* python3.6
* timeout

```sh
sudo port selfupdate
sudo port install gawk
sudo port install python36
sudo port select --set python3 python36
sudo port install coreutils
```
