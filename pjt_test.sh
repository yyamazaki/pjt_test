#!/bin/bash

# variable
declare -a list
declare -a p
declare -a rand_option_list

test_case=1
score=0
list=(`seq -f %02g 0 99`)
max_score=50
mkdir -p output
numL=0
tnumL=1
timemax=10
passing_rate=80

test_check(){
    Type=$1
    Num=$2
    Tnum=$3
    answer_name="./test_case/answer_${Type}${Num}.txt"
    result=1

    if [ -s ./diff.txt ]; then
        if [[ `grep -i -c "usage" ${answer_name}` != 0 && `grep -i -c "usage" ./output.txt` != 0 ]]; then
            echo -e "${Tnum}: ./pjt $4 \033[0;32mok!\033[0;39m"
        elif [[ `grep -i -c "no node" ${answer_name}` != 0 && `grep -i -c "no" ./output.txt` != 0 ]]; then
            echo -e "${Tnum}: ./pjt $4 \033[0;32mok!\033[0;39m" 
        else
            echo -e "${Tnum}: ./pjt $4 \033[0;31merror\033[0;39m"
            mv output.txt ./output/output_file${Tnum}.txt
            result=0
        fi
    else
        echo -e "${Tnum}: ./pjt $4 \033[0;32mok!\033[0;39m"
    fi

    return $result
}
BT_cut(){
    BinSTree_line=`cat output.txt | grep -n "^---BinSTree---$" | sed -e 's/:.*//g'`
    if [[ ${BinSTree_line} != "" ]]; then
        BinSTree_line_num=`expr ${BinSTree_line}-1`
	if [ $? -lt 2 ]; then
            awk "NR > $BinSTree_line_num" output.txt > tmp.txt
            mv tmp.txt output.txt
	fi
    fi
    BinSTree_line=`cat ./test_case/$1 | grep -n "^---BinSTree---$" | sed -e 's/:.*//g'`
    if [[ ${BinSTree_line} != "" ]]; then
        awk "NR > $((${BinSTree_line}-1))" ./test_case/$1 > tmp.txt
        mv tmp.txt output.txt
    fi
}

if [[ $# == 1 ]]; then
    if [[ -e ${@} ]]; then
        pjt_test_dir=`pwd`
        pjt_dir=`echo $(cd $(dirname $@) && pwd)/$(basename $@)`
        pjt_path=${pjt_dir}"/pjt"
    else
        echo "Error : No such directory"
        exit
    fi
else
    echo "Error : No PATH"
    echo "Usage : ./pjt_test.sh [option] PATH"
    echo "   -t : make test case"
    echo " PATH : pjt directory"
    exit
fi

# make
cd ${pjt_dir}
make -s

if [[ `ls | grep -i -c "^pjt$"` = 0 ]]; then
    echo "Error : No pjt"
    exit
fi

# cd pjt_test
cd ${pjt_test_dir}

# remove output file
if [[ `ls output | grep -i -c "^output"` != 0 ]]; then
    rm -rf ./output/output_*.txt
fi

# test
# file reading
cp ./test_case/-test_a.txt ./test_case/3 ./test_case/test_*.txt .
echo "start test in file reading"
while read line
do
    num="${list[${numL}]}"
    tnum="${list[${tnumL}]}"
    if [[ ${test_case} -eq 0 ]]; then
        ${pjt_path} ${line} > ./test_case/answer_file${num}.txt
        echo "${num}: ./pjt ${line}"
    else
        timeout ${timemax} ${pjt_path} ${line} > output.txt
        if [ $? -eq 124 ]; then
            echo "${tnum}: ./pjt ${line} \033[0;31merror because of timeout\033[0;39m"
        else
            BT_cut answer_file${num}.txt
            diff -u output.txt ./test_case/answer_file${num}.txt > diff.txt
            test_check file ${num} ${tnum} ${line}
            score=$(($score + $?))
        fi
    fi
    numL=$((${numL}+1))
    tnumL=$((${tnumL}+1))
done < ./test_case/testCases_file.txt

# file reading (random)
echo "\nstart test in file reading (random)"
if [[ ${test_case} -eq 0 ]]; then
    # making test_case
    for num in `seq -f %02g 0 $((${random_test_num} - 1))`
    do
        read_file="test_random"${num}".txt"
        rand_option_list=`rand_option`
        ${pjt_path} ${rand_option_list} ${read_file} > ./test_case/answer_file_random${num}.txt
        echo "${num}: ./pjt ${rand_option_list} ${read_file}"
        echo "${rand_option_list} ${read_file}" >> ./test_case/testCases_file_random.txt
    done
else
    numL=0
    while read line
    do
        num="${list[${numL}]}"
        tnum="${list[${tnumL}]}"
        timeout ${timemax} ${pjt_path} ${line} > output.txt
        if [ $? -eq 124 ]; then
            echo "${tnum}: ./pjt ${line} \033[0;31merror because of timeout\033[0;39m"
        else
            BT_cut answer_file_random${num}.txt
            diff -u output.txt ./test_case/answer_file_random${num}.txt > diff.txt
            test_check file_random ${num} ${tnum} ${line}
            score=$(($score + $?))
        fi
        numL=$((${numL}+1))
        tnumL=$((${tnumL}+1))
    done < ./test_case/testCases_file_random.txt
fi

# standard input
numL=0
echo "\nstart test in standard input"
while read line
do
    num="${list[${numL}]}"
    tnum="${list[${tnumL}]}"
    com=(${line})
    if [[ test_case -eq 0 ]]; then
        ${pjt_path} ${com[1,-3]} < ${com[-1]} > ./test_case/answer_stdin${num}.txt
        echo "${num}: ./pjt ${line}"
    else
        timeout ${timemax} ${pjt_path} ${com[@]:0:((${#com[@]}-2))} < ${com[@]: -1} > output.txt
        if [ $? -eq 124 ]; then
            echo "${tnum}: ./pjt ${line} \033[0;31merror because of timeout\033[0;39m"
        else
            BT_cut answer_stdin${num}.txt
            diff -u output.txt ./test_case/answer_stdin${num}.txt > diff.txt
            test_check stdin ${num} ${tnum} ${line}
            score=$(($score + $?))
        fi
    fi
    numL=$((${numL}+1))
    tnumL=$((${tnumL}+1))
done < ./test_case/testCases_stdin.txt

score_rate=$((${score} * 100 / ${max_score}))
if [[ ${test_case} -eq 1 ]]; then
    if [[ ${score_rate} -eq 100 ]]; then
        echo -e "\n\033[1;32mscore/max_score = ${score}/${max_score}\033[0m"
        echo -e "\n\033[0;32mtest complete!\033[0;39m"
        result=0
    elif [[ ${score_rate} -ge ${passing_rate} ]]; then
        echo -e "\n\033[1;33mscore/max_score = ${score}/${max_score}\033[0m"
        echo -e "\n\033[0;33mpassing test!\033[0;39m"
        result=0
    else
        echo -e "\n\033[1;31mscore/max_score = ${score}/${max_score}\033[0m"
        echo -e "\n\033[0;31mfailed test!\033[0;39m"
        result=1
    fi
fi

# directory clean in pjt_test and pjt
rm -rf ./-test_a.txt ./test_*.txt diff.txt ./3 output.txt
cd ${pjt_dir}
make -s clean

echo "Code coverage: ${score_rate}%"

exit $result
