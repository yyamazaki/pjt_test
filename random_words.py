#!/usr/bin/env python

import os
import argparse
import random
from distutils.util import strtobool


def create_random_words_file(output_file: str = None, candidate_str: str = '\n',
                             num_words: int = None, num_char: int = None, sep: str = ' \n'):
    """
    単語数 num_words までランダムな文字列で単語を生成し、output_file に出力する。

    num_words の指定がない場合、num_charで単語数ではなく文字数を指定する。

    num_words == num_char == None はエラー。

    :param output_file: 出力ファイル名。
    :param candidate_str: 単語生成に使われる候補文字列。
    :param num_words: 生成する最大単語数。
    :param num_char: 生成する文字数。
    :param sep: 区切り文字。単語数を数えるのに使用する。
    :return: output_file
    """

    if os.path.exists(output_file):
        over_write = input(f'"{output_file}" を上書きしますか? [y]/n: ')
        if not over_write:
            over_write = 'yes'
            print(over_write)
        if strtobool(over_write):
            with open(output_file, 'w') as f:
                pass
        else:
            raise FileExistsError(f'ファイル "{output_file}" がすでに存在しています。')

    with open(output_file, 'a') as f:
        if num_words:
            count_words = 0
            while count_words <= num_words:
                output_char = random.choice(candidate_str)
                if output_char in sep:
                    count_words += 1
                f.write(output_char)
        elif num_char:
            for _ in range(num_char):
                f.write(random.choice(candidate_str))
        else:
            raise RuntimeError('生成する単語数(--words)か文字数(--chars)を指定してください。')

    return output_file


def main():
    parser = argparse.ArgumentParser(description='最大単語数か文字数に応じた長さのランダムな文字列を生成する。')
    parser.add_argument('--output', '-o', type=str, default='result.txt', help='output file name')
    parser.add_argument('--candidate', '-c', type=str, default='abcdef \n', help='選択肢文字列 (default "abcdef \\n")')
    parser.add_argument('--sep', type=str, default=' \n', help='単語の区切り文字 (default 半角スペース、改行文字)')
    parser.add_argument('--words', '-w', type=int, default=None, help='最大生成単語数 (default None)')
    parser.add_argument('--chars', type=int, default=None, help='生成文字数 (default None)')

    args = parser.parse_args()

    try:
        create_random_words_file(args.output, args.candidate, args.words, args.chars, args.sep)
    except (ValueError, FileExistsError, RuntimeError) as e:
        print("Error:", e)
    else:
        print('complete!')


if __name__ == '__main__':
    main()
